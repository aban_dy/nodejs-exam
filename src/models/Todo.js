const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
	todo : String,
	userId : { type: Schema.Types.ObjectId, ref: 'User'}
},{ timestamps : true });

module.exports = mongoose.model('Todo', TodoSchema);