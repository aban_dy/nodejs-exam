const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserScehema = new Schema({
	name : String,
	email : String,
	password: String,
	isConfirmed: { type:Boolean, default:false },
	verificationToken : String
},{ timestamps : true });

module.exports = mongoose.model('User', UserScehema);