require('dotenv').config();
const jwt = require('jsonwebtoken');

module.exports = (req,res,next) => {
	const token = req.body.token;

	if(!token) return res.status(401).json({ message: "Access Denied" });

	try{
		let payload = jwt.verify(token, process.env.JWT_SECRET);
		req.user = payload;
		next();
	}catch(e){
		res.status(401).send("Unauthorized");
	}
}