require('dotenv').config();
const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const { sendConfirmationEmail } = require('../services/EmailService');

const UserRouter = express.Router();
const UserModel = require('../models/User');

//Validations
const reqister_validation = Joi.object().keys({
	name : Joi.string().min(2).max(30).required(),
	email: Joi.string().min(2).max(30).required(),
	password: Joi.string().min(4).max(24).required()
});

const login_validation = Joi.object().keys({
	email: Joi.string().trim().email().required(),
	password: Joi.string().min(4).max(24).required()
});


//Register route
UserRouter.post('/register', async(req,res) => {
	/*
		Validate user input
	*/
	const { error, result } = reqister_validation.validate(req.body);

	if( error ){ 
		res.status(400).json({ message: error[0].message }) 
	}else{
		let email = await UserModel.findOne({ email:req.body.email }); //Check if email is already taken
		if(email){
			res.status(400).json({ message: "Email already taken" });
		}else{
			let user = UserModel({
				name : req.body.name,
				email : req.body.email
			});

			/*
				Password hashing
			*/
			const salt = bcrypt.genSaltSync(10);
			const hashed = bcrypt.hashSync(req.body.password, salt);
			user.password = hashed; // Hashed password

			/*
				Adding Verification Token
			*/
			const verificationToken = jwt.sign({
			email: user.email
			}, process.env.JWT_SECRET, { expiresIn : '1d' });
			user.verificationToken = verificationToken;


			

			/*Saving User*/
			try{
				user = await user.save();
				/*Sending Verification Email*/
				sendConfirmationEmail(user);
				res.status(200).json({ message: "Registered Successfuly" });
			}catch(e){
				res.status(400).json({ message : "Error!, Please try again" })
			}

		}
	}
})

//Login route
UserRouter.post('/login', async(req,res) => {
	/*
		Validate user input
	*/
	const { error, result } = login_validation.validate(req.body);
	if(error){
		req.status(400).json({ message: "Incorrect email or passowrd" });
	}else{
		let user = await UserModel.findOne({ email:req.body.email });// Check if ther is an email registered

		if(!user){
			res.status(400).json({ message: "No user found" });
		}

		/*Check if password matches*/
		const matched = await bcrypt.compare(req.body.password, user.password);

		if(!matched){
			res.status(400).json({ message: "Incorrect email or password" });
		}

		if(user.isConfirmed){

			/*Creating token*/
			const token = jwt.sign({
				email: user.email,
				id: user._id
			}, process.env.JWT_SECRET, { expiresIn : 1800000 });

			/*Data that will be returned*/
			const loggedInUser = {
				token : token,
				user : {
					id : user._id,
					name: user.name,
					email: user.email
				}
			}

			res.send(loggedInUser);

		}else{
			res.status(401).json({ message: "Please confirm your email address" });
		}

		
	}
});

//Email confirmation route
UserRouter.get('/confirmation/:verificationToken', async(req,res) => {
	try{
	 const user = await UserModel.findOneAndUpdate({ verificationToken : req.params.verificationToken },{ isConfirmed : true },{ new: true });
	 if(user === null){
	 	res.status(401).json({ message: "Verification Expired" });
	 }else{
	 	res.status(200).json({ message: "Email Verified!" });
	 }
	}catch(e){
		console.log(e);
	}
});

module.exports = UserRouter;