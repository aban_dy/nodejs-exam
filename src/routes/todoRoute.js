const express = require('express');
const TodoModel = require('../models/Todo');
const TodoRouter = express.Router();
const auth = require('../middleware/login_middleware');

//CRUD

/*
	Create
*/
TodoRouter.post('/addTodo', auth, async(req,res) => {
	try{
		let todo = TodoModel({
			userId : req.body.userId,
			todo: req.body.todo
		});
		todo = await todo.save();
		res.send(todo);
	}catch(e){
		res.status(401).send("Unauthorized");
	}
});

/*
	Retrieve
*/
TodoRouter.post('/todos', auth, async(req,res) => {
	try{
		let todos = await TodoModel.find({ userId: req.body.userId});
		res.send(todos);
	}catch(e){
		res.status(401).send("Unauthorized");
	}
});

/*
	Update
*/
TodoRouter.patch('/updateTodo', auth, async(req,res) => {
	try{
		const update = {
			todo : req.body.todo
		}

		let updatedTodo = await TodoModel.findByIdAndUpdate(req.body.id, update, { new : true });
		res.send(updatedTodo);

	}catch(e){
		res.status(401).send("Unauthorized");
	}
})

/*
	Delete
*/
TodoRouter.delete('/deleteTodo', auth, async(req,res) => {
	try{
		const todo = await TodoModel.findByIdAndDelete(req.body.id);
		res.send(todo);
	}catch(e){
		res.status(401).send("Unauthorized");
	}
})

module.exports = TodoRouter;