require('dotenv').config();
const nodemailer = require('nodemailer');
const nodemailerSendgrid = require('nodemailer-sendgrid');
const transport = nodemailer.createTransport(
    nodemailerSendgrid({
        apiKey: process.env.SENDGRID_API_KEY
    })
);

/*Sending a Message*/
const sendConfirmationEmail = ( user ) => {

	const url = `http://localhost:3000/api/confirmation/${user.verificationToken}`;

	transport.sendMail({
	    from: 'mayiha6150@htwern.com',
	    to: `${user.name} <${user.email}>`,
	    subject: 'Confirmation Email',
	    html: `Confirmation Email <a href=${url}>${url}</a>`
	}).then(() => {
		console.log("Email sent");
	}).catch((e) => {
		console.log("Email not sent", e);
	})
}

exports.sendConfirmationEmail = sendConfirmationEmail;

