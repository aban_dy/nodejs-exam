**CRUD with login/register and email Verification**

Exam source code. TODO list

---
## Requirements

For development, you will only need Node.js and a node global package.

---

## Install

    $ git clone https://gitlab.com/aban_dy/nodejs-exam.git
    $ cd nodejs-exam
    $ npm install


## Running the project

    $ npm run dev
---

## APIS and Routes
1. Use http://localhost:3000/api endpoint
2. It uses port 3000 by default, change PORT in .env file if necessary
3. Use postman to test
---

## LOGIN
**/login**
> Provide email and password
```
Example:
{
"email" : "example@example.com",
"password" : "123123123"
}
```


## REGISTER
**/register**
> Provide name, email and password 
```
Example:
{
"name" : "Brandon",
"email" : "example@example.com",
"password" : "123123123"
}
```



## CRUD
> NOTE: All of the endpoints below need token to verify

**/addTodo**
```
example:
{
"token" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNzdnRsYmZ4d2Vucnd1aml0akB0dGlydi5uZXQiLCJpZCI6IjVlZTg3ZDM1ODZmNTgyM2M2ODVhYTY4OSIsImlhdCI6MTU5MjI5NTQ4OCwiZXhwIjoxNTk0MDk1NDg4fQ._vZFc6DLSj_SAaZOiMHy5OgCZTbCyzj5eLEFgshhpKc",
"userId" : "5ee87d3586f5823c685aa689",
"todo" : "Test todo"
}
```


**/todos**
```
example:
{
"token" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNzdnRsYmZ4d2Vucnd1aml0akB0dGlydi5uZXQiLCJpZCI6IjVlZTg3ZDM1ODZmNTgyM2M2ODVhYTY4OSIsImlhdCI6MTU5MjI5NTQ4OCwiZXhwIjoxNTk0MDk1NDg4fQ._vZFc6DLSj_SAaZOiMHy5OgCZTbCyzj5eLEFgshhpKc",
"userId" : "5ee87d3586f5823c685aa689"
}
```

    
**/updateTodo**
```
example:
{
"token" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNzdnRsYmZ4d2Vucnd1aml0akB0dGlydi5uZXQiLCJpZCI6IjVlZTg3ZDM1ODZmNTgyM2M2ODVhYTY4OSIsImlhdCI6MTU5MjI5NTQ4OCwiZXhwIjoxNTk0MDk1NDg4fQ._vZFc6DLSj_SAaZOiMHy5OgCZTbCyzj5eLEFgshhpKc",
"id" : "5ee8859b48b4290608296788",
"todo" : "Test todo update"
}
```

    
**/deleteTodo**
```
example:
{
"token" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNzdnRsYmZ4d2Vucnd1aml0akB0dGlydi5uZXQiLCJpZCI6IjVlZTg3ZDM1ODZmNTgyM2M2ODVhYTY4OSIsImlhdCI6MTU5MjI5NTQ4OCwiZXhwIjoxNTk0MDk1NDg4fQ._vZFc6DLSj_SAaZOiMHy5OgCZTbCyzj5eLEFgshhpKc",
"id" : "5ee8859b48b4290608296788"
}
```


