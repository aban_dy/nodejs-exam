require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose');


app.use(express.urlencoded({ extended:false }));

app.use(express.json());

app.use(cors());

//Database connection
mongoose.connect(process.env.DB_URL,{
	useNewUrlParser : true,
	useUnifiedTopology: true,
	useFindAndModify : false
}).then(() => {
	console.log(`Remote database connection successful`);
});

//PORT
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));

//APIS
const user = require('./src/routes/userRoute');
app.use('/api', user);

const todo = require('./src/routes/todoRoute');
app.use('/api', todo);
